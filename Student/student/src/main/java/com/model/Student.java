package com.model;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity

public class Student {
	@Id@GeneratedValue
	private int studentId;
	
	@Column
	private String studentName;
	private double fees;
 
	public Student(){
	
	
		super();
	}
	
	public Student(int studentId,String studentName, double fees){
		super();
		this.studentId = studentId;
		this.studentName = studentName;
		this.fees = fees;
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public double getFees() {
		return fees;
	}

	public void setFees(double fees) {
		this.fees = fees;
	}
	@Override
	public String toString(){
		return "Student [studentId="+studentId+", studentName = "+studentName+", fees = "+fees+"]";
	}
	
	
}