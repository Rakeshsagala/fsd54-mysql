import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-showempbyid',
  templateUrl: './showempbyid.component.html',
  styleUrl: './showempbyid.component.css'
})
export class ShowempbyidComponent implements OnInit {

  emailId: any;
  empId: any;
  emp: any;

  //Dependency Injection for EmpService
  constructor(private service: EmpService) {
    this.emailId = localStorage.getItem("emailId");   
  }

  ngOnInit() {
  }

  getEmployee() {
    this.emp = null;
    
    this.service.getEmployeeById(this.empId).subscribe((data: any) => {
      console.log(data);
      this.emp = data;
    });
  }

}