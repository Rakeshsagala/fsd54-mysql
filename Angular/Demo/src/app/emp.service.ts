import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EmpService {

  loginStatus: any;

  //Dependency Injection for HttpClient
  constructor(private http: HttpClient) { 
    this.loginStatus = false;
  }

  getAllCountries(): any {
    return this.http.get('https://restcountries.com/v3.1/all');
  }

  //When Login Success
  setLoginStatus() {
    this.loginStatus = true;
  }

  //For AuthGuard
  getLoginStatus(): boolean {
    return this.loginStatus;
  }

  //ForLogout
  setLogoutStatus() {
    this.loginStatus = false;
  }

  getEmployees(): any {
    return this.http.get('http://localhost:8085/getEmployees');
  }

  getEmployeeById(empId: any): any {
    return this.http.get('http://localhost:8085/getEmployeeById/' + empId);
  }

  getDepartments(): any {
    return this.http.get('http://localhost:8085/getDepartments');
  }


}