import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent implements OnInit {

  departments: any;
  countries: any;

  //Dependency Injection for EmpService
  constructor(private service: EmpService) {
  }

  ngOnInit() {
    //Fetching the countries data from EmpService
    this.service.getAllCountries().subscribe((data: any) => {this.countries = data;});
    this.service.getDepartments().subscribe((data: any) => {this.departments = data;});
  }

  registerSubmit(regForm: any) {
    console.log(regForm);
  }

}