import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent implements OnInit {

  emp: any;
  emailId: any;
  password: any;
  employees: any;

  //Dependency Injection for Router, EmpService
  constructor(private router: Router, private service: EmpService) {
    this.employees = [
      {empId:101, empName:'Rakesh',  salary:1212.12, gender:'Male',   doj:'05-25-2018', country:"IND", emailId:'rakesh@gmail.com',  password:'123'},
      {empId:102, empName:'Sai',   salary:2323.23, gender:'Male',   doj:'06-26-2017', country:"USA", emailId:'sai@gmail.com',   password:'123'},
      {empId:103, empName:'Rajesh',  salary:3434.34, gender:'Male', doj:'07-27-2016', country:"CHI", emailId:'rajesh@gmail.com',  password:'123'},
      {empId:104, empName:'Sourav',  salary:4545.45, gender:'Male',   doj:'08-28-2015', country:"JAP", emailId:'sourav@gmail.com',  password:'123'},
      {empId:105, empName:'Gandeev', salary:5656.56, gender:'Male',   doj:'09-29-2014', country:"UK",  emailId:'gandeev@gmail.com', password:'123'}
    ];
  }

  ngOnInit() {
  }

  loginSubmit(loginForm: any) {

    //Setting the emailId under localstorage
    localStorage.setItem("emailId", loginForm.emailId);

    //Remaining code is as it is available
  
    if(loginForm.emailId === "HR" && loginForm.password === "HR") {

      //Authentication the User to access the components through authguard
      this.service.setLoginStatus();
      this.router.navigate(['showemps']);
    } else {
      this.employees.forEach((element: any) => {
        if (element.emailId == loginForm.emailId && element.password == loginForm.password) {
          this.emp = element;
        }
      });

      if (this.emp != null) {
        
        //Authentication the User to access the components through authguard
        this.service.setLoginStatus();
        this.router.navigate(['products']);
      } else {
        alert('Invalid Credentials');
      }
    }


  }

  submit() {
    alert("EmailId: " + this.emailId + "\nPassword: " + this.password);
    console.log(this.emailId);
    console.log(this.password);
  }

}