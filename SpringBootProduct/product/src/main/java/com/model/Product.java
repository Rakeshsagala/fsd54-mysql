package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Product {
	
	//Entity : Making the normal class as an Entity Class
	//Id     : Applying the Primary Key Contraint to the respective column
	//GeneratedValue: Auto_Increment
	//Column : providing our own name for the column
	
	@Id@GeneratedValue
	private int productId;
	
	@Column(name="prodName")
	private String productName;
	
	private double price;
	
	public Product() {
		super();
	}

	public Product(int productId, String productName, double price) {
		super();
		this.productId = productId;
		this.productName = productName;
		this.price = price;
	}

	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}

	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Product [productId=" + productId + ", productName=" + productName + ", price=" + price + "]";
	}
}