package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Product;



@Service
public class ProductDAO {
	
	//Dependency Injection
	@Autowired
	ProductRepository productRepo;
	
	public List<Product> getProducts() {
		List<Product> prodList = productRepo.findAll();
		return prodList;
	}
	
	public Product getProductById(int prodId) {
		Product prod = new Product(0, "Product Not Found!!!", 0.0);
		Product product = productRepo.findById(prodId).orElse(prod);
		return product;
	}
	
	public Product getProductByName(String prodName) {
		return productRepo.findByName(prodName);
	}
	
	public Product addProduct(Product product) {
		return productRepo.save(product);
	}
	
	public Product updateProduct(Product product) {
		return productRepo.save(product);
	}
	
	public void deleteProductById(int prodId) {
		productRepo.deleteById(prodId);
	}

}
