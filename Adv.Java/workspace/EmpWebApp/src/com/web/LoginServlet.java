package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.EmployeeDAO;
import com.dto.Employee;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public LoginServlet() {
        super();
        
    }

	
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		String emailId = request.getParameter("emailId");
		String password = request.getParameter("password");
		
		//Creating the Session object to store emailId
		HttpSession session = request.getSession();
		session.setAttribute("emailId", emailId);
		
		out.print("<html><body bgcolor='lightyellow' text='blue'>");
		
		if (emailId.equalsIgnoreCase("HR") && password.equals("HR")) {	
		    RequestDispatcher rd = request.getRequestDispatcher("HRHomePage.jsp");
		    rd.forward(request, response);
		} else {
			
			EmployeeDAO empDAO = new EmployeeDAO();
			Employee emp = empDAO.empLogin(emailId, password);
			
			if (emp != null) {
				
			    //Storing the emp data into the session object for profile
			    session.setAttribute("emp", emp);
							
			    RequestDispatcher rd = request.getRequestDispatcher("EmpHomePage");
			    rd. forward(request, response);
			} else {
				out.print("<center><h1 style='color:red;'>Invalid Credentials</h1>");	
				
				RequestDispatcher rd = request.getRequestDispatcher("Login.html");
				rd.include(request, response);
			}						
		}
		out.print("</center></body></html>");
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
