package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.DepartmentDao;
import com.model.Department;

@RestController
public class DepartmentController {

	@Autowired
	DepartmentDao deptDao;
	
	@GetMapping("getDepartments")
	public List<Department> getDepartments() {
		return deptDao.getDepartments();
	}
	
	@GetMapping("getDepartmentById/{id}")
	public Department getDepartmentById(@PathVariable("id") int deptId) {
		return deptDao.getDepartmentById(deptId);
	}
	
	@GetMapping("getDepartmentByName/{name}")
	public Department getDepartmentByName(@PathVariable("name") String deptName) {
		return deptDao.getDepartmentByName(deptName);
	}
	
	@PostMapping("addDepartment")
	public Department addDepartment(@RequestBody Department department) {
		return deptDao.addDepartment(department);
	}
	
	@PutMapping("updateDepartment")
	public Department updateDepartment(@RequestBody Department department) {
		return deptDao.updateDepartment(department);
	}
	
	@DeleteMapping("deleteById/{id}")
	public String deleteDepartmentById(@PathVariable("id") int deptId) {
		deptDao.deleteDepartmentById(deptId);
		return "Department with DepartmentId: " + deptId + ", Deleted Successfully";
	}
	
}
