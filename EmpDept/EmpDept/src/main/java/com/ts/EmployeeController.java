package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.EmployeeDao;
import com.model.Employee;

@RestController
public class EmployeeController {

	@Autowired
	EmployeeDao empDAO;
	
	@GetMapping("getEmployees")
	public List<Employee> getEmployees() {
		return empDAO.getEmployees();
	}
	
	@GetMapping("getEmployeeById/{id}")
	public Employee getEmployeeById(@PathVariable("id") int empId) {
		return empDAO.getEmployeeById(empId);
	}
	
	@GetMapping("getEmployeeByName/{name}")
	public Employee getEmployeeByName(@PathVariable("name") String empName) {
		return empDAO.getEmployeeByName(empName);
	}
	
	@PostMapping("addEmployee")
	public Employee addEmployee(@RequestBody Employee employee) {
		return empDAO.addEmployee(employee);
	}
	
	@PutMapping("updateEmployee")
	public Employee updateEmployee(@RequestBody Employee employee) {
		return empDAO.updateEmployee(employee);
	}
	
	@DeleteMapping("deleteEmployee")
	public Employee deleteEmployee(@RequestBody Employee employee) {
		return empDAO.deleteEmployee(employee);
	}
	
}
