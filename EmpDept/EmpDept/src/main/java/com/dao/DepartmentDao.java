package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Department;

@Service
public class DepartmentDao {
	
	@Autowired
	DepartmentRepository deptRepo;

	public List<Department> getDepartments() {
		return deptRepo.findAll();
	}

	public Department getDepartmentById(int deptId) {
		return deptRepo.findById(deptId).orElse(null);
	}

	public Department getDepartmentByName(String deptName) {
		return deptRepo.findByName(deptName);
	}
	
	public Department addDepartment(Department department) {
		return deptRepo.save(department);
	}
	
	public Department updateDepartment(Department department) {
		return deptRepo.save(department);
	}
	
	public void deleteDepartmentById(int deptId) {
		deptRepo.deleteById(deptId);
	}
}
